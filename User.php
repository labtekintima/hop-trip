<?php
class User {
	/*
	* @return string
     */
	public function postSignUp() {
		//init
		$db = new db();
		$id = 0; $resp = 0;
		//var_dump($_POST);
		$uname = (isset($_POST['username'])) ? $_POST['username'] : "";
		$pass = (isset($_POST['password'])) ? $_POST['password'] : "";
		$email = (isset($_POST['email'])) ? $_POST['email'] : "";
		//cek username dan email sudah ada belum
		if (($uname!="")&&($pass!="")&&($email!="")){
			$pass = md5($pass);
			$query = "SELECT * FROM User WHERE username = '$uname' OR email = '$email'";
			$res = $db->query($query);
			$isAda = (count($res)>=1) ? true : false;
			if (!$isAda){
				//kalau belum dimasukkan
				$id = 0;
				$query = "INSERT INTO User (username,password,email,poin,level,reward,bio) VALUES ('$uname','$pass','$email',0,0,0,'')";
				$db->exec($query);
				$id = $db->lastInsertId();
				$resp = ($id==0) ? 0 : 1;
			}else
				$resp = -1;
				//kalau sudah ada
		}else
			$resp = 0;
			
		$ret = array(
			"response" => $resp,
			"id" => $id,
		);
		
		return json_encode($ret);
	}
	/*
	* @return string
     */
	public function postLogIn() {
		//init
		//var_dump($_POST);
		$db = new db();
		$resp = 0;
		//var_dump($_POST);
		$uname = $_POST['username'];
		$pass = md5($_POST['password']);
		//cek login
		$query = "SELECT * FROM User WHERE username = '$uname' AND password = '$pass'";
		$res = $db->query($query);
		$resp = (count($res)>=1) ? 1 : 0;
		$id = $res[0]['idUser'];
		
		$ret = array(
			"response" => $resp,
			"id" => $id,
		);
		
		return json_encode($ret);
	}
	 /**
     * Auto routed method that creates all possible routes.
     * This was the standard behavior for Restler 2
     *
     * @smart-auto-routing false
     */
	public function getProfile($uid) {
		//init
		$db = new db();
		$resp = 0;
	
		//get profile
		$query = "SELECT nama,username,email,poin,level,reward,bio,thumbfoto FROM User WHERE idUser = $uid";
		$user = $db->query($query);
		$resp = (count($user)>=1) ? 1 : 0;
		if ($resp){
			$user = $user[0];
			$thumb = $user['thumbfoto'];
			//get trip done
			$query = "SELECT t.idTrip,t.judul,t.coverFoto,t.ratings,t.totaljarak,t.danatiket,t.danamakan,t.durasi,td.timestamp FROM
				TripDone td INNER JOIN Trip t on td.idTrip = t.idTrip
				WHERE td.isDone = 1 AND td.idUser = $uid";
			//echo $query;
			$trips = $db->query($query);
			$tripdone = array();
			foreach ($trips as $trip){
				$done = array();
				$done['idTrip'] = $trip['idTrip'];
				$done['judul'] = $trip['judul'];
				$done['coverfoto'] = $trip['coverFoto'];
				$done['thumbfoto'] = $thumb;
				$done['ratings'] = $trip['ratings'];
				$done['rating_dana'] = rateDana($trip['danatiket']+$trip['danamakan']);
				$done['rating_jarak'] = rateJarak($trip['totaljarak']);
				$done['rating_duration'] = rateDurasi($trip['durasi']);
				$done['timestamp'] = $trip['timestamp'];
				array_push($tripdone,$done);
			}
			$user['tripsdone'] = $tripdone;
			//get trip made
			$query = "SELECT * FROM Trip WHERE idCreator = $uid";
			//echo $query;
			$trips = $db->query($query);
			$tripmade = array();
			foreach ($trips as $trip){
				$made = array();
				$made['idTrip'] = $trip['idTrip'];
				$made['judul'] = $trip['judul'];
				$made['coverfoto'] = $trip['coverFoto'];
				$made['thumbfoto'] = $thumb;
				$made['ratings'] = $trip['ratings'];
				$made['rating_dana'] = rateDana($trip['danatiket']+$trip['danamakan']);
				$made['rating_jarak'] = rateJarak($trip['totaljarak']);
				$made['rating_duration'] = rateDurasi($trip['durasi']);
				array_push($tripmade,$made);
			}
			$user['tripsmade'] = $tripmade;
		}
		//
		$ret = array(
			"response" => $resp,
			"data" => $user
		);
		
		return json_encode($ret);
	}
	/*
	* @return string
     */
	public function postOwnProfile() {
		//init
		$db = new db();
		$resp = 0;
		//
		$uid = $_POST['userid'];
		$pass = md5($_POST['password']);
		//get user
		$query = "SELECT nama,username,email,poin,level,reward,bio,thumbfoto FROM User WHERE idUser = $uid AND password = '$pass'";
		$user = $db->query($query);
		$resp = (count($user)>=1) ? 1 : 0;
		if ($resp){
			$user = $user[0];
			$thumb = $user['thumbfoto'];
			//get badges
			$query = "SELECT * FROM Badges WHERE idUser = $uid";
			$badges = $db->query($query);
			$user['badges'] = $badges;
			//get rewards
			$query = "SELECT gr.idGetReward,gr.timestamp,r.hadiah,p.nama FROM GetReward gr
				INNER JOIN Reward r ON gr.idReward = r.idReward
				INNER JOIN Place p ON r.idPlace = p.idPlace
				WHERE gr.idUser = $uid";
			//echo $query;
			$rewards = $db->query($query);
			$user['rewards'] = $rewards;
		}
		//	
		$ret = array(
			"response" => $resp,
			"data" => $user
		);
		
		return json_encode($ret);
	}
}

<?php
class Place {
	/**
     * Auto routed method that creates all possible routes.
     * This was the standard behavior for Restler 2
     *
     * @smart-auto-routing false
     */
	function postCheckin($tripid,$placeid) {
		//init
		$db = new db();
		$resp = 0;
		//
		$lat = $_POST['lat'];
		$lon = $_POST['lon'];
		$uid = $_POST['userid'];
		//cek dengan latitude di dbase apakah dalam lingkaran
		$query = "SELECT
				  p.idPlace 
				  ,(
					6371 * acos (
					  cos ( radians($lat) )
					  * cos( radians( p.lat ) )
					  * cos( radians( p.lon ) - radians($lon) )
					  + sin ( radians($lat) )
					  * sin( radians( p.lat ) )
					)
				  ) AS distance
				FROM TripPlaces tp
				INNER JOIN Place p ON tp.idPlace = p.idPlace
				INNER JOIN Trip t ON t.idTrip = tp.idTrip
				WHERE t.idCreator = p.idUser AND t.idTrip = $tripid AND p.idPlace = $placeid
				HAVING distance <= 1
				ORDER BY distance ASC";	
		//echo $query;
		$place = $db->query($query);
		$checked = (count($place)>=1) ? 1 : 0;
		if ($checked){
			//kalau ya, last visit dan timestamp aktifkan
			$place = $place[0];
			$idPlace = $place['idPlace'];
			$query = "UPDATE User SET lastv_time = NOW(), lastv_place = $idPlace WHERE idUser = $uid";
			$db->exec($query);
			$ret = $db->rowCount();
			//echo $query;
			$checked = (count($ret)>=1) ? 1 : 0;
		}else{
			//kalau tidak ya tidak
		}
		//	
		$ret = array(
			"response" => $checked
		);
		
		return json_encode($ret);
	}
	/**
     * Auto routed method that creates all possible routes.
     * This was the standard behavior for Restler 2
     *
     * @smart-auto-routing false
     */
	function postAdd() {
		//init
		$db = new db();
		$resp = 0;
		//
		$nama = amankan($_POST['nama']);
		$kota = $_POST['kota'];
		$lat = $_POST['lat'];
		$lon = $_POST['lon'];
		$uid = $_POST['userid'];
		//cek dengan latitude di dbase apakah dalam lingkaran
		//var_dump($_POST);
		//cek apakah sudah ada
		$query = "SELECT * FROM Place WHERE nama = '$nama' AND ((ABS(ABS(lat) - ABS($lat)) < 0.00000000005) AND (ABS(ABS(lon) - ABS($lon)) < 0.00000000005))";
		//echo $query;
		$res = $db->query($query);
		$isAda = (count($res)>=1) ? true : false;
		if (!$isAda){
			//masukkan
			$query = "INSERT INTO Place (idUser,nama,lat,lon,kota) VALUES ($uid,'$nama',$lat,$lon,'$kota')";	
			//echo $query;
			$place = $db->exec($query);
			$id = $db->lastInsertId();
			$resp = ($id==0) ? 0 : 1;
		}else{
			$resp = 1;
			$id = $res[0]['idPlace'];
		}
		if ($resp){
			//kalau ya, last visit dan timestamp aktifkan
			$query = "UPDATE User SET lastv_time = NOW(), lastv_place = $id WHERE idUser = $uid";
			$db->exec($query);
			$ret = $db->rowCount();
			//echo $query;
			$resp = (count($ret)>=1) ? 1 : 0;
		}else{
			//kalau tidak ya tidak
		}
		//	
		$ret = array(
			"response" => $resp
		);
		
		return json_encode($ret);
	}
	/**
     * Auto routed method that creates all possible routes.
     * This was the standard behavior for Restler 2
     *
     * @smart-auto-routing false
     */
	function getSaved($uid) {
		//init
		$db = new db();
		$resp = 0;
		//
		/*$query = "SELECT p.nama,p.keterangan,p.lat,p.lon,p.kota,pf.fotourl FROM Place p
					LEFT JOIN PlaceFoto pf ON p.idPlace = pf.idPlace
					INNER JOIN Trip t ON t.idCreator = p.idUser
					WHERE p.idUser = $uid AND pf.isThumb = 1 AND pf.idTrip = t.idTrip";*/
		$query = "SELECT p.idPlace,p.nama,p.keterangan,p.lat,p.lon,p.kota,pf.fotourl FROM Place p
					LEFT JOIN PlaceFoto pf ON p.idPlace = pf.idPlace
					WHERE p.idUser = $uid";
		//echo $query;
		$places = $db->query($query);
		$resp = (count($places)>=1) ? 1 : 0;
		$ret = array(
			"response" => $resp,
			"data" => $places
		);
		
		return json_encode($ret);
	}
	/**
     * Auto routed method that creates all possible routes.
     * This was the standard behavior for Restler 2
     *
     * @smart-auto-routing false
     */
	function getSponsored() {
		//init
		$db = new db();
		$resp = 0;
		//
		$query = "SELECT p.nama,p.keterangan,p.lat,p.lon,p.kota,pf.fotourl,r.hadiah FROM Place p
					INNER JOIN Reward r ON p.idSponsor = r.idSponsor
					INNER JOIN PlaceFoto pf ON p.idPlace = pf.idPlace
					INNER JOIN Trip t ON t.idSponsor = p.idSponsor
					WHERE pf.isThumb = 1 AND pf.idTrip = t.idTrip";
		$places = $db->query($query);
		$resp = (count($places)>=1) ? 1 : 0;
		$ret = array(
			"response" => $resp,
			"data" => $places
		);
		
		return json_encode($ret);
	}
	/**
     * Auto routed method that creates all possible routes.
     * This was the standard behavior for Restler 2
     *
     * @smart-auto-routing false
     */
	function getSuggest() {
		//init
		$db = new db();
		$resp = 0;
		//
		$lat = $_GET['lat'];
		$lon = $_GET['lon'];
		//
		$url = "http://api.foursquare.com/v2/venues/search?ll=$lat,$lon&radius=100&limit=10&intent=browse&client_id=FJWPOZ2AQB3OZABDYZJMI1WQHBQBJWLAQHO5M1CKBSN4RBTO&client_secret=NCKBSSHHRXDOBIQBEEJ1NF5LJAPGJZPABCIO1LDHWMV0ANTK&v=20140707";
		//echo $url;
		$curlHandle = curl_init();
		curl_setopt($curlHandle, CURLOPT_URL, $url);
		curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curlHandle, CURLOPT_USERAGENT, 'Zenziva SMS Curled RealiTA');
		curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
		curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
		$result = curl_exec($curlHandle);
		curl_close($curlHandle);
		//var_dump(json_decode($result, true));
		//
		$ret = array(
			"response" => $resp,
			"data" => $places
		);
		
		//return json_encode($ret);
	}
}

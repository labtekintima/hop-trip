<?php
class Sponsor {
	/**
     * Auto routed method that creates all possible routes.
     * This was the standard behavior for Restler 2
     *
     * @smart-auto-routing false
     */
	function getPlaces($sid) {
		//init
		$db = new db();
		$resp = 0;
		//
		$query = "SELECT r.idPlace,p.nama,p.lat,p.lon,p.kota,r.hadiah,r.nama AS namareward,r.keterangan,r.anggaran,r.visitors FROM Reward r
			INNER JOIN Place p ON r.idPlace = p.idPlace
			WHERE r.idSponsor = $sid";
		$places = $db->query($query);
		$resp = (count($places)>=1) ? 1 : 0;
		//	
		$ret = array(
			"response" => $resp,
			"data" => $places
		);
		
		return json_encode($ret);
	}
}

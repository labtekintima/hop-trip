SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`User`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`User` (
  `idUser` INT NOT NULL AUTO_INCREMENT ,
  `username` VARCHAR(15) NULL ,
  `password` VARCHAR(32) NULL ,
  `email` VARCHAR(45) NULL ,
  `idfb` TEXT NULL ,
  `idgoogle` TEXT NULL ,
  `poin` INT NULL DEFAULT 0 ,
  `level` INT NULL DEFAULT 0 ,
  `reward` INT NULL ,
  `lastv_time` DATETIME NULL ,
  `lastv_place` INT NULL ,
  `bio` TEXT NULL ,
  PRIMARY KEY (`idUser`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`TripDone`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`TripDone` (
  `idTripDone` INT NOT NULL AUTO_INCREMENT ,
  `idUser` INT NOT NULL ,
  `idTrip` INT NOT NULL ,
  `timestamp` TIMESTAMP NOT NULL ,
  `rating` INT NULL ,
  `isDone` INT NOT NULL ,
  PRIMARY KEY (`idTripDone`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`GetReward`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`GetReward` (
  `idGetReward` INT NOT NULL AUTO_INCREMENT ,
  `idUser` INT NOT NULL ,
  `idReward` INT NOT NULL ,
  `timestamp` TIMESTAMP NOT NULL ,
  PRIMARY KEY (`idGetReward`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Trip`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`Trip` (
  `idTrip` INT NOT NULL AUTO_INCREMENT ,
  `judul` VARCHAR(45) NOT NULL ,
  `filters` VARCHAR(7) NULL ,
  `keterangan` TEXT NULL ,
  `durasi` INT NOT NULL ,
  `waktumulai` TIME NOT NULL ,
  `danatiket` INT NULL ,
  `danamakan` INT NULL ,
  `totaljarak` FLOAT NULL ,
  `ratings` INT NULL ,
  `idCreator` INT NULL ,
  `idSponsor` INT NULL ,
  `start_lat` FLOAT NULL ,
  `start_lon` FLOAT NULL ,
  `disqus_url` TEXT NULL ,
  `thumbFoto` TEXT NULL ,
  PRIMARY KEY (`idTrip`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Badges`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`Badges` (
  `idBadges` INT NOT NULL AUTO_INCREMENT ,
  `idUser` INT NULL ,
  `jenisbadge` INT NULL ,
  `timestamp` TIMESTAMP NULL ,
  PRIMARY KEY (`idBadges`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Place`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`Place` (
  `idPlace` INT NOT NULL AUTO_INCREMENT ,
  `idUser` INT NULL ,
  `idSponsor` INT NULL ,
  `nama` VARCHAR(50) NULL ,
  `keterangan` TEXT NULL ,
  `lat` FLOAT NULL ,
  `lon` FLOAT NULL ,
  `kota` VARCHAR(45) NULL ,
  PRIMARY KEY (`idPlace`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`PlaceFoto`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`PlaceFoto` (
  `idPlaceFoto` INT NOT NULL AUTO_INCREMENT ,
  `idPlace` INT NULL ,
  `fotourl` TEXT NULL ,
  `isThumb` INT NULL ,
  PRIMARY KEY (`idPlaceFoto`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`TripPlaces`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`TripPlaces` (
  `idTripPlaces` INT NOT NULL AUTO_INCREMENT ,
  `idTrip` INT NULL ,
  `idPlace` INT NULL ,
  `urutan` INT NULL ,
  `keterangan` TEXT NULL ,
  `durasi` INT NULL ,
  `waktumulai` TIME NULL ,
  `danatiket` INT NULL ,
  `danamakan` INT NULL ,
  PRIMARY KEY (`idTripPlaces`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Journey`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`Journey` (
  `idJourney` INT NOT NULL AUTO_INCREMENT ,
  `jeniskendaraan` INT NULL ,
  `keterangan` TEXT NULL ,
  `durasi` INT NULL ,
  `waktumulai` TIME NULL ,
  `danatiket` INT NULL ,
  `placea` INT NULL ,
  `placeb` INT NULL ,
  PRIMARY KEY (`idJourney`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Sponsor`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`Sponsor` (
  `idSponsor` INT NOT NULL AUTO_INCREMENT ,
  `nama` VARCHAR(45) NULL ,
  `logourl` TEXT NULL ,
  `username` VARCHAR(45) NULL ,
  `password` VARCHAR(45) NULL ,
  `keterangan` TEXT NULL ,
  PRIMARY KEY (`idSponsor`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Reward`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`Reward` (
  `idReward` INT NOT NULL AUTO_INCREMENT ,
  `idSponsor` INT NULL ,
  `idPlace` INT NULL ,
  `visitors` INT NULL ,
  `anggaran` INT NULL ,
  `hadiah` INT NULL ,
  `nama` VARCHAR(45) NULL ,
  `keterangan` TEXT NULL ,
  PRIMARY KEY (`idReward`) )
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

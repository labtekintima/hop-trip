<?php

function slugging($string) {
	$nstring = preg_replace("/[^a-zA-Z0-9]/","-",strtolower($string));
	return $nstring;
}

function rateDana($val){
	$val = (int)$val;
	switch($val){
		case ($val < 50000) :
			return 5;
			break;
		case ($val >= 50000 && $val < 100000) :
			return 4;
			break;
		case ($val >= 100000 && $val < 200000) :
			return 3;
			break;
		case ($val >= 200000 && $val < 250000) :
			return 2;
			break;
		case ($val > 250000) :
			return 1;
			break;
		default:
			return 1;
			break;
	}
}

function rateJarak($val){
	$val = (int)$val;
	switch($val){
		case ($val < 10) :
			return 5;
			break;
		case ($val >= 10 && $val < 15) :
			return 4;
			break;
		case ($val >= 15 && $val < 20) :
			return 3;
			break;
		case ($val >= 20 && $val < 30) :
			return 2;
			break;
		case ($val > 30) :
			return 1;
			break;
		default:
			return 1;
			break;
	}
}

function rateDurasi($val){
	$val = (int)$val;
	$jam = 3600;
	switch($val){
		case ($val < ($jam*2)) :
			return 5;
			break;
		case ($val >= ($jam*2) && $val < ($jam*4)) :
			return 4;
			break;
		case ($val >= ($jam*4) && $val < ($jam*8)) :
			return 3;
			break;
		case ($val >= ($jam*8) && $val < ($jam*10)) :
			return 2;
			break;
		case ($val > ($jam*10)) :
			return 1;
			break;
		default:
			return 1;
			break;
	}
}

function distanceGeoPoints ($lat1, $lng1, $lat2, $lng2) {
		$earthRadius = 3958.75;

		$dLat = deg2rad($lat2-$lat1);
		$dLng = deg2rad($lng2-$lng1);


		$a = sin($dLat/2) * sin($dLat/2) +
		   cos(deg2rad($lat1)) * cos(deg2rad($lat2)) *
		   sin($dLng/2) * sin($dLng/2);
		$c = 2 * atan2(sqrt($a), sqrt(1-$a));
		$dist = $earthRadius * $c;

		// from miles
		$meterConversion = 1.609;
		$geopointDistance = $dist * $meterConversion;

		return $geopointDistance;
}

function amankan($str) {
	// experimental
	if (get_magic_quotes_gpc()) {
		return $str;
	} else {
		if (function_exists('addslashes')) {
			return addslashes($str);
		} else {
			return mysql_real_escape_string($str);
		}
	}
}

function placeAdd($nama,$kota,$lat,$lon,$uid) {
	//init
	$db = new db();
	$resp = 0;
	//
	//cek dengan latitude di dbase apakah dalam lingkaran
	//var_dump($_POST);
	//cek apakah sudah ada
	$nama = slugging($nama);
	$kota = slugging($kota);
	$query = "SELECT * FROM Place WHERE nama = '$nama' AND ((ABS(ABS(lat) - ABS($lat)) < 0.00000000005) AND (ABS(ABS(lon) - ABS($lon)) < 0.00000000005))";
	$res = $db->query($query);
	$isAda = (count($res)>=1) ? true : false;
	if (!$isAda){
		//masukkan
		$query = "INSERT INTO Place (idUser,nama,lat,lon,kota) VALUES ($uid,'$nama',$lat,$lon,'$kota')";	
		//echo $query;
		$place = $db->exec($query);
		$id = $db->lastInsertId();
		$resp = ($id==0) ? 0 : 1;
	}else{
		$resp = 1;
		$id = $res[0]['idPlace'];
	}
	//echo "idplace = $id";
	return $id;
}

function placeFotoAdd($idTrip,$idPlace,$imgurl,$isThumb = 1) {
	//init
	$db = new db();
	$resp = 0;
	//
	$query = "SELECT * FROM PlaceFoto WHERE idTrip = $idTrip AND idPlace = $idPlace";
	$res = $db->query($query);
	$isAda = (count($res)>=1) ? true : false;
	if (!$isAda){
		//masukkan
		$query = "INSERT INTO PlaceFoto (idTrip,idPlace,fotourl,isThumb) VALUES ($idTrip,$idPlace,'$imgurl',$isThumb)";	
		//echo $query;
		$place = $db->exec($query);
		$id = $db->lastInsertId();
	}else{
		$query = "UPDATE PlaceFoto SET fotourl = '$imgurl' WHERE idTrip = $idTrip AND idPlace = $idPlace";	
		//echo $query;
		$place = $db->exec($query);
		$id = $db->lastInsertId();
	}
	//echo "idplace = $id";
	return $id;
}

function tripplaceAdd($tripid,$placeid,$urut,$ket,$durasi,$waktu,$tiket,$makan) {
	//init
	$db = new db();
	$resp = 0;
	//
	//masukkan
	$query = "INSERT INTO TripPlaces (idTrip,idPlace,urutan,keterangan,durasi,waktumulai,danatiket,danamakan) VALUES ($tripid,$placeid,$urut,'$ket',$durasi,'$waktu',$tiket,$makan)";	
	//echo $query;
	$place = $db->exec($query);
	$id = $db->lastInsertId();
	$resp = ($id==0) ? 0 : 1;
	
	return $resp;
}

function tripAdd($judul,$filters,$ket,$durasi,$waktu,$tiket,$makan,$jarak,$ratings,$uid,$lat,$lon,$cover) {
	//init
	$db = new db();
	$resp = 0;
	//
	//masukkan
	$ket = slugging($ket);
	$query = "INSERT INTO Trip (judul,filters,keterangan,durasi,waktumulai,danatiket,danamakan,totaljarak,ratings,idCreator,start_lat,start_lon,coverFoto) VALUES ('$judul','$filters','$ket',$durasi,'$waktu',$tiket,$makan,$jarak,$ratings,$uid,$lat,$lon,'$cover')";	
	//echo $query;
	$place = $db->exec($query);
	$id = $db->lastInsertId();
	$resp = ($id==0) ? 0 : 1;
	
	return $id;
}

function journeyAdd($tripid,$jenisK,$ketJ,$durasiJ,$waktuJ,$tiketJ,$placeid,$placeidb) {
	//init
	$db = new db();
	$resp = 0;
	//
	//masukkan
	$ketJ = slugging($ketJ);
	$query = "INSERT INTO Journey (idTrip,jeniskendaraan,keterangan,durasi,waktumulai,danatiket,placea,placeb) VALUES ($tripid,$jenisK,'$ketJ',$durasiJ,'$waktuJ',$tiketJ,$placeid,$placeidb)";	
	//echo $query;
	$place = $db->exec($query);
	$id = $db->lastInsertId();
	$resp = ($id==0) ? 0 : 1;
	
	return $id;
}

function tripDone($uid,$tripid,$rate) {
	//init
	$db = new db();
	$resp = 0;
	//
	//masukkan
	$id = 0;
	$query = "INSERT INTO TripDone (idUser,idTrip,timestamp,rating,isDone) VALUES ($uid,$tripid,NOW(),$rate,1)";
	$db->exec($query);
	$id = $db->lastInsertId();
	$resp = ($id==0) ? 0 : 1;
	
	return $id;
}

function base64_to_jpeg( $base64_string, $output_file ) {
    list($type, $data) = explode(';', $base64_string);
	list(, $data)      = explode(',', $data);
	//
    $ifp = fopen( $output_file, "wb" ); 
    fwrite( $ifp, base64_decode( $data) ); 
    fclose( $ifp ); 
    return( $output_file );
}

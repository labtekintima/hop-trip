<?php
class Trip {
	/**
     * Auto routed method that creates all possible routes.
     * This was the standard behavior for Restler 2
     *
     * @smart-auto-routing false
     */
	function getList($lat,$lon) {
		//init
		$db = new db();
		$resp = 0;
		//
		$filter = (isset($_GET['filter'])) ? $_GET['filter'] : '';
		$filter = urldecode($filter);
		$qword = (isset($_GET['qword'])) ? $_GET['qword'] : '';
		$query = "SELECT
				  t.idTrip, t.judul, t.filters, t.coverFoto, u.thumbfoto, s.logourl, t.ratings, t.totaljarak, t.danamakan, t.danatiket, t.durasi, td.isDone, t.start_lat, t.start_lon,
				  (
					6371 * acos (
					  cos ( radians($lat) )
					  * cos( radians( t.start_lat ) )
					  * cos( radians( t.start_lon ) - radians($lon) )
					  + sin ( radians($lat) )
					  * sin( radians( t.start_lat ) )
					)
				  ) AS distance
				FROM Trip t
				LEFT JOIN User u ON t.idCreator = u.idUser
				LEFT JOIN Sponsor s ON t.idSponsor = s.idSponsor
				LEFT JOIN TripDone td ON td.idTrip = t.idTrip ";
		$filters = explode(',',$filter);
		$qwords = explode(',',$qword);
		if ((count($filters)>0 && $filters[0]!='')||(count($qwords)>0 && $qwords[0]!='')){
			$query .= "WHERE ";
			if (count($filters)>0 && $filters[0]!=''){
				$query .= "(";
				for ($i=0; $i<(count($filters)-1); $i++){
					$query .= "filters LIKE '%".$filters[$i]."%' OR ";
				}
				$query .= "filters LIKE '%".$filters[$i]."%' ";
				$query .= ") ";
			}
			if (count($qwords)>0 && $qwords[0]!=''){
				if (count($filters)>0 && $filters[0]!='') $query .= "AND (";
					else
				$query .= " (";
				for ($i=0; $i<(count($qwords)-1); $i++){
					$query .= "judul LIKE '%".$qwords[$i]."%' OR ";
				}
				$query .= "judul LIKE '%".$qwords[$i]."%' ";
				$query .= ") ";
			}				
		}
		$query .= "HAVING distance <= 10 ORDER BY distance ASC";	
		//echo $query;
		$trips = $db->query($query);
		$resp = (count($trips)>=1) ? 1 : 0;
		$tripss = array();
		foreach ($trips as $trip){
			$ent = array();
			$ent['idTrip'] = $trip['idTrip'];
			$ent['judul'] = $trip['judul'];
			$ent['filters'] = $trip['filters'];
			$ent['coverfoto'] = $trip['coverFoto'];
			$ent['thumbfoto'] = ($trip['thumbfoto']=='') ? $trip['logourl'] : $trip['thumbfoto'];
			$ent['ratings'] = $trip['ratings'];
			$ent['rating_dana'] = rateDana($trip['danatiket']+$trip['danamakan']);
			$ent['rating_jarak'] = rateJarak($trip['totaljarak']);
			$ent['rating_duration'] = rateDurasi($trip['durasi']);
			$ent['dana'] = $trip['danatiket']+$trip['danamakan'];
			$ent['jarak'] = $trip['totaljarak'];
			$ent['duration'] = $trip['durasi'];
			$ent['isWish'] = $trip['isDone'];
			$ent['lat'] = $trip['start_lat'];
			$ent['lon'] = $trip['start_lon'];
			array_push($tripss,$ent);
		}
		//	
		$ret = array(
			"response" => $resp,
			"data" => $tripss
		);
		
		return json_encode($ret);
	}
	/**
     * Auto routed method that creates all possible routes.
     * This was the standard behavior for Restler 2
     *
     * @smart-auto-routing false
     */
	function getView($tripid) {
		//init
		$db = new db();
		$resp = 0;
		//
		$query = "SELECT t.judul, t.filters, t.coverFoto, u.thumbfoto, s.logourl, t.ratings, t.totaljarak, t.danamakan, t.danatiket, t.durasi, t.waktumulai, t.keterangan, t.start_lat, t.start_lon
				FROM Trip t
				LEFT JOIN User u ON t.idCreator = u.idUser
				LEFT JOIN Sponsor s ON t.idSponsor = s.idSponsor
				WHERE t.idTrip = $tripid";
		//echo $query;
		$trip = $db->query($query);
		$resp = (count($trip)>=1) ? 1 : 0;
		//
		$data = array();
		//data trip
		if ($resp){
			$trip = $trip[0];
			$data['judul'] = $trip['judul'];
			$data['filters'] = $trip['filters'];
			$data['coverfoto'] = $trip['coverFoto'];
			$data['thumbfoto'] = ($trip['thumbfoto']=='') ? $trip['logourl'] : $trip['thumbfoto'];
			$data['ratings'] = $trip['ratings'];
			$data['rating_dana'] = rateDana($trip['danatiket']+$trip['danamakan']);
			$data['rating_jarak'] = rateJarak($trip['totaljarak']);
			$data['rating_duration'] = rateDurasi($trip['durasi']);
			$data['dana'] = $trip['danatiket']+$trip['danamakan'];
			$data['danatiket'] = $trip['danatiket'];
			$data['danamakan'] = $trip['danamakan'];
			$data['jarak'] = $trip['totaljarak'];
			$data['durasi'] = $trip['durasi'];
			$data['waktumulai'] = $trip['waktumulai'];
			$data['keterangan'] = $trip['keterangan'];
			$data['lat'] = $trip['start_lat'];
			$data['lon'] = $trip['start_lon'];
		}	
		//data places
		$query = "SELECT tp.idPlace, tp.urutan, tp.keterangan, tp.durasi, tp.waktumulai, tp.danatiket, tp.danamakan, p.nama, p.lat, p.lon FROM TripPlaces tp
			INNER JOIN Place p ON p.idPlace = tp.idPlace
			WHERE tp.idTrip = $tripid ORDER BY urutan ASC";
		$places = $db->query($query);
		$res= (count($trip)>=1) ? 1 : 0;
		$placea = array();
		foreach ($places as $place){
			$ent = array();
			//DATA
			$ent = $place;
			//FOTO
			$query = "SELECT * FROM PlaceFoto WHERE idTrip = $tripid AND idPlace = ".$ent['idPlace'];
			$fotos = $db->query($query);
			$res= (count($trip)>=1) ? 1 : 0;
			$ent['foto'] = array();
			foreach ($fotos as $foto){
				if ($foto['isThumb']) $ent['thumbfoto'] = $foto['fotourl'];
				array_push($ent['foto'],$foto['fotourl']);
			}
			array_push($placea,$ent);
		}
		$data['places'] = $placea;
		//data journey
		$journeya = array();
		for ($i=0; $i < count($data['places'])-1; $i++){
			$place_a = $data['places'][$i]['idPlace'];
			$place_b = $data['places'][$i+1]['idPlace'];
			$query = "SELECT * FROM Journey WHERE idTrip = $tripid AND placea = $place_a AND placeb = $place_b";
			$journey = $db->query($query);
			array_push($journeya,$journey[0]);
		}
		$data['journeys'] = $journeya;
		//
		$ret = array(
			"response" => $resp,
			"data" => $data
		);
		
		return json_encode($ret);
	}
	/**
     * Auto routed method that creates all possible routes.
     * This was the standard behavior for Restler 2
     *
     * @smart-auto-routing false
     */
	function postRec($uid) {
		$resp = 0;
		//ambil data dari device
		$tripa = json_decode($_POST['tripadd']);
		//var_dump($tripa);
		//hitung dll
		$judul = $tripa->trip->judul;
		$ket = $tripa->trip->ket;
		//durasi masih tricky
		$waktu = $tripa->places[0]->time;
		$waktua = $tripa->places[count($tripa->places)-1]->time;
		$durasi=(int) ((strtotime($waktua) - strtotime($waktu))/60);
		//tiket-makan
		$tiket = 0; $makan = 0;
		foreach($tripa->trippl as $trippl){
			$tiket += $trippl->tiket;
			$makan += $trippl->makan;
		}
		$tags = '';
		//jarak dari gmaps
		$jarak = 0;
		foreach($tripa->places as $key => $place){
			if ($key>0){
				$lata = $tripa->places[$key]->lat;
				$lona = $tripa->places[$key]->lon;
				$base_url = 'http://maps.googleapis.com/maps/api/directions/xml?sensor=false&mode=walking';
				$xml = simplexml_load_file("$base_url&origin=$lat,$lon&destination=$lata,$lona");
				$jaraks = (int)($xml->route->leg->distance->value/1000);
				$jarak += $jaraks;
			}
			$lat = $tripa->places[$key]->lat;
			$lon = $tripa->places[$key]->lon;
		}
		//
		$cover = $tripa->trip->cover;
		//masukkin trip
		$tripid = tripAdd($judul,$tags,$ket,$durasi,$waktu,$tiket,$makan,$jarak,0,$uid,$lat,$lon,$cover);
		//masukkin places dan tripplace, journey
		foreach($tripa->places as $key => $place){
			$ket=""; $durasi=0; $tiket=0; $makan=0;
			if ($key>0 && $key<=count($tripa->places)){
				//tripplace ??
				$urut = $key;
				$ket = $tripa->trippl[$key-1]->ket;
				$tiket = (int) ($tripa->trippl[$key-1]->tiket);
				$makan = (int) ($tripa->trippl[$key-1]->makan);
				if ($key<count($tripa->places)){
					$waktua = $tripa->places[$key]->time;
					$durasi=(int) ((strtotime($waktua) - strtotime($waktu))/60);
				}else
					$durasi = (int) ($tripa->trippl[$key-1]->durasi);
				tripplaceAdd($tripid,$placeid,$urut,$ket,$durasi,$waktu,$tiket,$makan);
				//journey ??
			}
			//
			$nama = $place->nama;
			$kota = (isset($place->kota)) ? $place->kota : '';
			$lat = $place->lat;
			$lon = $place->lon;
			$waktu = $place->time;
			$placeid = placeAdd($nama,$kota,$lat,$lon,$uid);
		}
		//masukkin trip done
		$resp = tripDone($uid,$tripid,0);
		//
		$ret = array(
			"response" => $resp
		);
		
		return json_encode($ret);
	}
	/**
     * Auto routed method that creates all possible routes.
     * This was the standard behavior for Restler 2
     *
     * @smart-auto-routing false
     */
	function postMake($uid,$done=0) {
		$resp = 0;
		//ambil data dari device
		$tripa = json_decode($_POST['tripadd']);
		//var_dump($tripa); die();
		//hitung dll
		$judul = $tripa->judul;
		$ket = $tripa->ket;
		//durasi masih tricky
		$waktu = $tripa->jamSt.":".$tripa->menitSt.":00";
		$waktua = $tripa->jamEnd.":".$tripa->menitEnd.":00";
		$durasi=(int) ((strtotime($waktua) - strtotime($waktu))/60);
		//tiket-makan
		$tiket = $tripa->tiket; $makan = $tripa->makan;
		//filters
		$tags = implode(",", $tripa->tags);
		//coverfoto
		$imgurl = "";
		if (strcmp($tripa->pic,"")!=0){
			$imgurl = 'img/trip/'.slugging($judul).".jpg";
			$my_base64_string = $tripa->pic;
			$image = base64_to_jpeg($my_base64_string, $imgurl);
		}
		//brings
		$brings = implode(",", $tripa->brings);
		//jarak dari gmaps
		$jarak = 0;
		foreach($tripa->places as $key => $place){
			if ($key>0){
				$lata = $tripa->places[$key]->lat;
				$lona = $tripa->places[$key]->lon;
				$base_url = 'http://maps.googleapis.com/maps/api/directions/xml?sensor=false&mode=walking';
				$xml = simplexml_load_file("$base_url&origin=$lat,$lon&destination=$lata,$lona");
				//echo "$base_url&origin=$lat,$lon&destination=$lata,$lona"."<br />";
				$jaraks = (int)($xml->route->leg->distance->value/1000);
				//echo "jaraks=".$jaraks."<br />";
				$jarak += $jaraks;
			}
			$lat = $tripa->places[$key]->lat;
			$lon = $tripa->places[$key]->lon;
		}
		$lat = $tripa->places[0]->lat;
		$lon = $tripa->places[0]->lon;
		//masukkin trip
		$tripid = tripAdd($judul,$tags,$ket,$durasi,$waktu,$tiket,$makan,$jarak,0,$uid,$lat,$lon,$imgurl);
		//masukkin places dan tripplace, journey
		foreach($tripa->places as $key => $place){
			$ket=""; $durasi=0; $tiket=0; $makan=0;
			//place
			$nama = $place->judul;
			$kota = (isset($place->kota)) ? $place->kota : '';
			$lat = $place->lat;
			$lon = $place->lon;
			$waktu = $place->jamSt.":".$place->menitSt.":00";
			$placeidb = placeAdd($nama,$kota,$lat,$lon,$uid);
			//placefoto
			$imgurl;
			if (strcmp($place->pic,"")!=0){
				$imgurl = 'img/placetrip/'.slugging($placeidb."-".$nama).".jpg";
				$my_base64_string = $place->pic;
				$image = base64_to_jpeg($my_base64_string, $imgurl);
			}
			placeFotoAdd($tripid,$placeidb,$imgurl,1);
			//tripplace ??
			$urut = $key+1;
			$ket = $place->ket;
			$tiket = (int) ($place->tiket);
			$makan = (int) ($place->makan);
			$waktua = $place->jamEnd.":".$place->menitEnd.":00";
			$durasi=(int) ((strtotime($waktua) - strtotime($waktu))/60);
			tripplaceAdd($tripid,$placeidb,$urut,$ket,$durasi,$waktu,$tiket,$makan);
			if ($key>0 && $key<=count($tripa->places)){
				//journey ??
				$jenisK = 1;
				$ketJ = "Fitur belum diaktifkan";
				$durasiJ = 0;
				$waktuJ = $waktu;
				$tiketJ = 0;
				journeyAdd($tripid,$jenisK,$ketJ,$durasiJ,$waktuJ,$tiketJ,$placeid,$placeidb);
			}
			$placeid = $placeidb;
			$resp = 1;
		}
		//masukkin trip done
		if ($done)
			$resp = tripDone($uid,$tripid,0);
		//
		$ret = array(
			"response" => $resp
		);
		
		return json_encode($ret);
	}
	/**
     * Auto routed method that creates all possible routes.
     * This was the standard behavior for Restler 2
     *
     * @smart-auto-routing false
     */
	function postEnd($tripid) {
		//init
		$db = new db();
		$resp = 0;
		//
		$uid = isset($_POST['userid']) ? $_POST['userid'] : 0;
		$rate = isset($_POST['rating']) ? $_POST['rating'] : 0;
		//cek wishlist
		$query = "SELECT * FROM TripDone WHERE idUser = $uid AND idTrip = $tripid";
		$trip = $db->query($query);
		$res = (count($trip)>=1) ? 1 : 0;
		if ($res){
			//kalau ada
			$query = "UPDATE TripDone SET isDone = 1, rating = $rate WHERE idUser = $uid AND idTrip = $tripid";
			$db->exec($query);
			$ret = $db->rowCount();
			//echo $query;
			$resp = (count($ret)>=1) ? 1 : 0;
		}else{
			//kalau tidak ada
			$id = 0;
			$query = "INSERT INTO TripDone (idUser,idTrip,timestamp,rating,isDone) VALUES ($uid,$tripid,NOW(),$rate,1)";
			$db->exec($query);
			$id = $db->lastInsertId();
			$resp = ($id==0) ? 0 : 1;
		}
		//
		$ret = array(
			"response" => $resp
		);
		
		return json_encode($ret);
	}
	/**
     * Auto routed method that creates all possible routes.
     * This was the standard behavior for Restler 2
     *
     * @smart-auto-routing false
     */
	function postAddwish($tripid) {
		//init
		$db = new db();
		$resp = 0;
		//
		$uid = isset($_POST['userid']) ? $_POST['userid'] : 0;
		//cek wishlist
		$query = "SELECT * FROM TripDone WHERE idUser = $uid AND idTrip = $tripid";
		$trip = $db->query($query);
		$res = (count($trip)>=1) ? 0 : 1;
		if ($res){
			//kalau belum ada
			$id = 0;
			$query = "INSERT INTO TripDone (idUser,idTrip,timestamp,rating,isDone) VALUES ($uid,$tripid,NOW(),0,0)";
			$db->exec($query);
			$id = $db->lastInsertId();
			$resp = ($id==0) ? 0 : 1;
		}
		//
		$ret = array(
			"response" => $resp
		);
		
		return json_encode($ret);
	}
	/**
     * Auto routed method that creates all possible routes.
     * This was the standard behavior for Restler 2
     *
     * @smart-auto-routing false
     */
	function getDone($uid) {
		//init
		$db = new db();
		$resp = 0;
		$filter = (isset($_GET['filter'])) ? $_GET['filter'] : '';
		$filter = urldecode($filter);
		$qword = (isset($_GET['qword'])) ? $_GET['qword'] : '';
		//
		/*
		 $query = "SELECT td.timestamp, t.idTrip, t.judul, t.filters, t.keterangan, t.durasi, t.danatiket, t.danamakan, t.totaljarak, t.ratings, u.thumbfoto, s.logourl, t.coverfoto, td.isDone
			FROM TripDone td
			INNER JOIN Trip t ON td.idTrip = t.idTrip
			LEFT JOIN User u ON u.idUser = t.idCreator
			LEFT JOIN Sponsor s ON s.idSponsor = t.idSponsor
			WHERE isDone = 1
			ORDER BY timestamp DESC
			";
		*/
		$query = "SELECT td.timestamp, t.idTrip, t.judul, t.filters, t.keterangan, t.durasi, t.danatiket, t.danamakan, t.totaljarak, t.ratings, u.thumbfoto, s.logourl, t.coverfoto, td.isDone
			FROM TripDone td
			INNER JOIN Trip t ON td.idTrip = t.idTrip
			LEFT JOIN User u ON u.idUser = t.idCreator
			LEFT JOIN Sponsor s ON s.idSponsor = t.idSponsor ";
		$filters = explode(',',$filter);
		$qwords = explode(',',$qword);
		if ((count($filters)>0 && $filters[0]!='')||(count($qwords)>0 && $qwords[0]!='')){
			$query .= "WHERE ";
			if (count($filters)>0 && $filters[0]!=''){
				$query .= "(";
				for ($i=0; $i<(count($filters)-1); $i++){
					$query .= "filters LIKE '%".$filters[$i]."%' OR ";
				}
				$query .= "filters LIKE '%".$filters[$i]."%' ";
				$query .= ") ";
			}
			if (count($qwords)>0 && $qwords[0]!=''){
				$query .= "AND (";
				for ($i=0; $i<(count($qwords)-1); $i++){
					$query .= "judul LIKE '%".$qwords[$i]."%' OR ";
				}
				$query .= "judul LIKE '%".$qwords[$i]."%' ";
				$query .= ") ";
			}				
		}
		$query .= "ORDER BY timestamp DESC";
		//echo $query;
		$trips = $db->query($query);
		$resp = (count($trips)>=1) ? 1 : 0;
		//
		$tripa = array();
		//data trip
		foreach ($trips as $trip) {
			$data = array();
			$data['idTrip'] = $trip['idTrip'];
			$data['judul'] = $trip['judul'];
			$data['filters'] = $trip['filters'];
			$data['coverfoto'] = $trip['coverfoto'];
			$data['thumbfoto'] = ($trip['thumbfoto']=='') ? $trip['logourl'] : $trip['thumbfoto'];
			$data['ratings'] = $trip['ratings'];
			$data['rating_dana'] = rateDana($trip['danatiket']+$trip['danamakan']);
			$data['rating_jarak'] = rateJarak($trip['totaljarak']);
			$data['rating_duration'] = rateDurasi($trip['durasi']);
			$data['dana'] = $trip['danatiket']+$trip['danamakan'];
			$data['jarak'] = $trip['totaljarak'];
			$data['duration'] = $trip['durasi'];
			$ent['isDone'] = $trip['isDone'];
			$data['timestamp'] = $trip['timestamp'];
			array_push($tripa,$data);
		}	
		//
		$ret = array(
			"response" => $resp,
			"data" => $tripa
		);
		
		return json_encode($ret);
	}
	/**
     * Auto routed method that creates all possible routes.
     * This was the standard behavior for Restler 2
     *
     * @smart-auto-routing false
     */
	function getMade($uid) {
		//init
		$db = new db();
		$resp = 0;
		//
		$query = "SELECT t.idTrip, t.judul, t.filters, t.keterangan, t.durasi, t.danatiket, t.danamakan, t.totaljarak, t.ratings, u.thumbfoto, t.coverfoto
			FROM Trip t
			LEFT JOIN User u ON u.idUser = t.idCreator
			WHERE t.idCreator = $uid
			ORDER BY idTrip DESC
			";
		$trips = $db->query($query);
		$resp = (count($trips)>=1) ? 1 : 0;
		//
		$tripa = array();
		//data trip
		foreach ($trips as $trip) {
			$data = array();
			$data['idTrip'] = $trip['idTrip'];
			$data['judul'] = $trip['judul'];
			$data['filters'] = $trip['filters'];
			$data['coverfoto'] = $trip['coverFoto'];
			$data['thumbfoto'] = ($trip['thumbfoto']=='') ? $trip['logourl'] : $trip['thumbfoto'];
			$data['ratings'] = $trip['ratings'];
			$data['rating_dana'] = rateDana($trip['danatiket']+$trip['danamakan']);
			$data['rating_jarak'] = rateJarak($trip['totaljarak']);
			$data['rating_duration'] = rateDurasi($trip['durasi']);
			$data['dana'] = $trip['danatiket']+$trip['danamakan'];
			$data['jarak'] = $trip['totaljarak'];
			$data['duration'] = $trip['durasi'];
			array_push($tripa,$data);
		}	
		//
		$ret = array(
			"response" => $resp,
			"data" => $tripa
		);
		
		return json_encode($ret);
	}
}
